BBSim2 

To be used/modified freely (See licence file), but please refer to the source (Gitlab and date downloaded) and Author (J.J. Janse van Rensburg). 

BBSim2 is intended for **Flexible or Rigid** rotor simulation including (**Active Magnetic Bearing**) AMB and (**Backup Bearing**) BB components with **contact modelling**
All modifications to be pushed back to a GIT branch

Contact details of author
Jan Janse van Rensburg
jjansev@mathworks.com

If you have any suggestions to improve the model please create a Issue on Gitlab or improve the feature yourself and make a merge request.

Requirements to run BBSim2:

0. MATLAB 2019a (or newer) & Simulink & SimScape & Simscape Multibody
1. Install - Simscape Multibody Contact Forces Library (https://www.mathworks.com/matlabcentral/fileexchange/47417-simscape-multibody-contact-forces-library)
2. Install - Simscape Multibody Parts Library (https://www.mathworks.com/matlabcentral/fileexchange/36536-simscape-multibody-parts-library)

Toolboxes used in simulation:
aerospace_blockset
aerospace_toolbox
matlab
rf_blockset
signal_blocks
simmechanics (Simscape Multibody)
simscape
simulink
simulink_test